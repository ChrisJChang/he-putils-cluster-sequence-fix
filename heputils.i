%module heputils

%{
#include "HEPUtils/Utils.h"
#include "HEPUtils/Vectors.h"
#include "HEPUtils/Particle.h"
#include "HEPUtils/Jet.h"
#include "HEPUtils/Event.h"
#include "HEPUtils/BinnedFn.h"
//#include "HEPUtils/PhaseSpace.h"
//#include "HEPUtils/MathUtils.h"
//#include "HEPUtils/FastJet.h"
%}

%ignore *::operator =;
%rename(mkP4) *::operator const P4&;
%ignore *::operator const P4*;
%ignore *::Particle::Particle; //(HEPUtils::Particle *);
%ignore *::Event::cloneTo; //(HEPUtils::Event &);
%copyctor *::Particle::Particle;

%include <std_string.i>
%include <std_vector.i>
%include <std_map.i>
%naturalvar;

%include "HEPUtils/Utils.h"
%include "HEPUtils/Vectors.h"
%include "HEPUtils/Particle.h"
%include "HEPUtils/Jet.h"
%include "HEPUtils/Event.h"
%include "HEPUtils/BinnedFn.h"
//%include "HEPUtils/PhaseSpace.h"
//%include "HEPUtils/FastJet.h"
//%include "HEPUtils/MathUtils.h"

%extend HEPUtils::P4 {
  std::string __repr__() const {
    std::ostringstream out;
    out << *$self;
    return std::string(out.str());
  }
}

%extend HEPUtils::P4 {
  HEPUtils::P4 __add__(const HEPUtils::P4& other) const {
    return *$self + other;
  }
}

// %extend HEPUtils::P4 {
//   HEPUtils::P4 __iadd__(const HEPUtils::P4& other) {
//     *$self += other;
//     return *$self;
//   }
// }

%extend HEPUtils::P4 {
  HEPUtils::P4 __sub__(const HEPUtils::P4& other) const {
    return *$self - other;
  }
}

// %extend HEPUtils::P4 {
//   HEPUtils::P4 __isub__(const HEPUtils::P4& other) {
//     *$self -= other;
//     return *$self;
//   }
// }

%extend HEPUtils::P4 {
  HEPUtils::P4 __mul__(double a) const {
    return a * *$self;
  }
}

%extend HEPUtils::P4 {
  HEPUtils::P4 __rmul__(double a) const {
    return a * *$self;
  }
}
