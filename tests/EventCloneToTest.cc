#include "HEPUtils/Event.h"
#include "TestTools.h"


int main() {

  HEPUtils::P4 vec, vec2;

  double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double px, py, pz, m;

  int particle_id = 22; //arbitrarily chosen
  bool promptness = false;
  bool psort = true;
  HEPUtils::Event ernie, bert;

  for (int i=0; i<10; i++) {
    px = unif(generator);
    py = unif(generator);
    pz = unif(generator);
    m = unif(generator);


    if (m < 0) {
      i--;
      continue;
    }
    vec.setPM(px, py, pz, m);

    HEPUtils::Particle* p = new HEPUtils::Particle(vec, particle_id);
    ernie.add_particle( p, psort);
  }

  ernie.cloneTo(bert);

  for (int i=0; i<ernie.particles().size(); i++) {
    HEPUtils::P4 fv1 = ernie.particles()[i]->mom();
    HEPUtils::P4 fv2 = bert.particles()[i]->mom();
    if (fv1.E() != fv2.E()) {
      return 1;
    }
  }
  return 0;
}
