#include "HEPUtils/Vectors.h"
#include "TestTools.h"


double ptrans2(HEPUtils::P4 v1) {
  return HEPUtils::sqr(v1.px()) + HEPUtils::sqr(v1.py());
}


int main() {

  double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double dot3_product, dot3_manual;
  double p1x, p1y, p1z, m1;

  HEPUtils::P4 vec;

  for (int i=0; i<10000; i++) {
    p1x = unif(generator);
    p1y = unif(generator);
    p1z = unif(generator);
    m1 = unif(generator);


    if (m1 < 0) {
      i--;
      continue;
    }

    vec = vec.setPM(p1x, p1y, p1z, m1);

    if (!almost_equal(vec.pT2(), ptrans2(vec), 1)) {
      return 1;
    }
  }
  return 0;
}
