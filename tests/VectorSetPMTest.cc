#include "HEPUtils/Vectors.h"
#include "TestTools.h"


int main() {

  const double lower_bound = -10000, upper_bound = 10000;
  std::uniform_real_distribution<double> unif(lower_bound, upper_bound);
  std::default_random_engine generator(time(0));
  HEPUtils::P4 vec;

  for (int i=0; i<10000; i++) {
    const double random_m = unif(generator);
    if (random_m < 0) {
      i -= 1;
      continue;
    }

    const double random_px = unif(generator);
    const double random_py = unif(generator);
    const double random_pz = unif(generator);

    vec.setPM(random_px, random_py, random_pz, random_m);
    double p_sqr = pow(random_px,2) + pow(random_py,2) + pow(random_pz,2);
    double random_E = sqrt(p_sqr + pow(random_m,2));

    if (!almost_equal(vec.E(), random_E, 1)) return 1;
  }

  return 0;
}
