#include "HEPUtils/Vectors.h"
#include "TestTools.h"


double angleBetween(HEPUtils::P4 v1, HEPUtils::P4 v2) {
  return acos(v1.dot3(v2) / (v1.p() * v2.p()));
}


int main() {

  double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double dot3_product, dot3_manual;
  double p1x, p1y, p1z, m1, p2x, p2y, p2z, m2;

  HEPUtils::P4 vec1, vec2;

  for (int i=0; i<10000; i++) {
    p1x = unif(generator);
    p1y = unif(generator);
    p1z = unif(generator);
    m1 = unif(generator);

    p2x = unif(generator);
    p2y = unif(generator);
    p2z = unif(generator);
    m2 = unif(generator);

    if (m1 < 0 || m2 < 0) {
      i--;
      continue;
    }

    vec1 = vec1.setPM(p1x, p1y, p1z, m1);
    vec2 = vec2.setPM(p2x, p2y, p2z, m2);

    dot3_product = vec1.dot3(vec2);

    if (!almost_equal(vec1.angleTo(vec2), angleBetween(vec1, vec2), 10000)) {
      return 1;
    }
  }
  return 0;
}
