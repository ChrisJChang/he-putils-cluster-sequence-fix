#include "HEPUtils/Vectors.h"
#include "TestTools.h"


inline double rapidity(HEPUtils::P4 vec) {
  return 0.5 * log((vec.E() + vec.pz()) / (vec.E() - vec.pz()));
}


int main() {

  double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double px, py, pz, m;
  HEPUtils::P4 vec;
  double rapfunc;
  double rap;

  for (int i=0; i<1000; i++) {
    px = unif(generator);
    py = unif(generator);
    pz = unif(generator);
    m = unif(generator);

    if (m < 0) {
      i--;
      continue;
    }

    vec.setPM(px, py, pz, m);
    rapfunc = rapidity(vec);
    rap = vec.rap();

    if ( !almost_equal(rapfunc, rap, 0.1)) {
      return 1;
    }
  }

  return 0;
}
